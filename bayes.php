<?php 
	session_start();

	if(!isset($_SESSION['user_id'])){
		header('location:login.php');
	}
	
  include dirname(__FILE__)."\php\connection.php"; //Info de conexão

  if( isset($_GET['e']) && isset($_GET['id'])){
	$resultSet = $db->query("
    SELECT 
    <marcas.id>,
    <marcas.nome>,
    <marcas.timestamp_changed>,
    <user.name> as <userchanged>
    FROM <marcas>
    LEFT JOIN <user> ON <marcas.user_changed> = <user.id>
	WHERE <marcas.id> = " . $_GET['id']
  )->fetchAll();
  }else{
	$resultSet = $db->query("
    SELECT 
    <marcas.id>,
    <marcas.nome>,
    <marcas.timestamp_changed>,
    <user.name> as <userchanged>
    FROM <marcas>
    LEFT JOIN <user> ON <marcas.user_changed> = <user.id>"
  )->fetchAll(); 
  }

  $ambientes = $db->query("
    SELECT 
    <ambientes.id>,
    <ambientes.nome>
    FROM <ambientes>"
  )->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Portal de Pricing versão 3.0">
  <meta name="author" content="Produtividade e Perfomance D2C ">

  <title>Portal de Pricing D2C</title>

  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

  

</head>

<body class="fixed-nav sticky-footer bg-dark " id="page-top">
  
<?php include "php/navigation.php";?>  

  
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">Bayes</li>
      </ol>
      
      <!-- CONTEÚDO -->
      <div class="conteudo">
	  
	      <div class="text-center">
          <h3>Arquivo Bayes atual</h3>
          <br><h4><a href="downloaded/bayes/summary.csv" target="blank">summary.csv</a></h4>
          
          <?php
            $filename = 'downloaded/bayes/summary.csv';
            if (file_exists($filename)) {
                echo date (" d/m/Y H:i:s", filemtime($filename));
            }
          ?>
        </div>
        <br><Br>
        <hr>
        <form action="bayes_upload.php" method="post" enctype="multipart/form-data">
          <div class="text-center">
            <h5>Enviar novo arquivo:</h5><br>
            <input type="file" name="fileToUpload" id="fileToUpload"><br><br>
            <input type="submit" value="Upload" name="submit">
          </div>
        </form>
          
        <br><br><hr>
          <div class="text-center">
            <h3>Layout do CSV Bayes</h3><br><br>
            <table style="width:100%" class="text-center">
              <tr>
                <th>Nulo</th>
                <th>SKU</th> 
                <th>Preço Ótimo</th>
                <th>Id do ambiente</th>
              </tr>
              <tr>
                <td>null</td>
                <td>BRE80AKANA</td> 
                <td>1230.00</td>
                <td>
                  <?php foreach($ambientes as $ambiente): ?>
                  <?php echo $ambiente['id']; ?> para <?php echo $ambiente['nome']; ?><br>
                  <?php endforeach; ?>
                  
                </td>
              </tr>
            </table>

            <br><br><h5>IMPORTANTE</h5><br><br> - Não inserir cabeçalho/títulos de colunas no arquivo;<br>- Campos devem ser separados por ponto e vírgula;<br>- Primeira coluna deve conter a palavra 'null'.<br><br><br>
        </div>
      </div>
      

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Desenvolvido por: Produtividade e Performance | Compra Certa | D2C </small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>


    <!-- Modal Sucesso -->
    <div class="modal fade" id="modalSucesso" tabindex="-1" role="dialog" aria-labelledby="labelModalSucesso" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModalSucesso">Upload Bayes</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="false">×</span>
            </button>
          </div>
          <div class="modal-body">
            Arquivo enviado com sucesso!          
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript -->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-charts.min.js"></script>

    <!-- ATIVA O MODAL DE SUCESSO NO CADASTRO -->
    <?php if(isset($_GET['s'])) : ?>
      <script type="text/javascript">
        $(document).ready(function(){
          $("#modalSucesso").modal('show');
      });
      </script>
    <?php endif; ?>

    <!-- Ativa a Tabela -->
    <script type="text/javascript">

      $(document).ready(function() {
        $('#tabela').DataTable({
          "scrollX": true
        });
      });
    </script>

  </div>
</body>

</html>
