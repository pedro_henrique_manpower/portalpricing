<?php
	$target_dir = "downloaded/bayes/";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$target_file = $target_dir . "summary.csv";
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


	// Allow certain file formats
	if($imageFileType != "csv" ) {
	    echo "Somente arquivos CSV podem ser enviados. ";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Arquivo não enviado.";

	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

	        // Copia o arquivo para a pasta histórico
			copy("downloaded/bayes/summary.csv","downloaded/bayes/historico/summary(". date (" d-m-Y H.i.s") . ").csv"); 
			header("Location: bayes.php?s=1");
			
	    } else {
	        echo "Erro ao fazer upload.";
	    }
	}


?>