-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pricing
CREATE DATABASE IF NOT EXISTS `pricing` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pricing`;

-- Dumping structure for table pricing.ambientes
CREATE TABLE IF NOT EXISTS `ambientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_url` text,
  `nome` text,
  `user_changed` int(11) DEFAULT NULL,
  `timestamp_changed` text,
  `vtex_key` text,
  `vtex_token` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table pricing.ambientes: 7 rows
/*!40000 ALTER TABLE `ambientes` DISABLE KEYS */;
INSERT INTO `ambientes` (`id`, `api_url`, `nome`, `user_changed`, `timestamp_changed`, `vtex_key`, `vtex_token`) VALUES
	(1, 'http://compracerta.vtexcommercestable.com.br', 'Compra Certa', 1, '2018-07-22 17:16:46', 'vtexappkey-compracerta-PGLJLX', 'SNWGYYVWLKATTEEQDANNHSIZSAPDHNAARBICXTSOWPGMXWVUCXOZQWUAEWOXISOXJABRVBTJIYQSLWBVFUBJZBKVHIYAFVGEAJDXUDYADFRSFBOIUSSNQEXIPGJNDTDN'),
	(2, 'http://whirlpool.vtexcommercestable.com.br', 'Whirlpool', 1, '2018-07-22 17:31:36', 'vtexappkey-whirlpool-FKGBAU', 'VDFCIBRODVRMWUAGZSAZUVIBBFKZUCSAUMWSZCUWWFXOKBLIXYCMDBMAUDBDRSUACQSKXIGXYEDOXURZTBBVAGWTZVXWBQEDLWMTJAKPREOZXXCAMBJPQYAVTYKBKKWH'),
	(3, 'http://kitchenaid.vtexcommercestable.com.br', 'KitchenAid', 2, '2018-07-22 19:06:51', 'aaaa', 'nbb'),
	(4, 'wer', 'wfe', 2, '2018-07-22 19:08:09', 'wef', 'wef'),
	(5, 'erg', 'erg', 2, '2018-07-22 19:11:13', 'erg', 'erg'),
	(6, 'http://wergfrewgf', 'Sony', 2, NULL, '123456789', 'eurghuigterg'),
	(7, 'bunda', 'bunda', 2, NULL, 'bunda', 'bunda');
/*!40000 ALTER TABLE `ambientes` ENABLE KEYS */;

-- Dumping structure for table pricing.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` int(11) DEFAULT NULL,
  `timestamp` text,
  `user_id` int(11) DEFAULT NULL,
  `mensagem` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;

-- Dumping data for table pricing.log: 169 rows
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` (`id`, `operacao`, `timestamp`, `user_id`, `mensagem`) VALUES
	(1, 1, '2018-09-02 19:18:59', 1, 'Iniciando processo de precificação.'),
	(2, 1, '2018-09-02 19:19:28', 1, 'Iniciando processo de precificação.'),
	(3, 1, '2018-09-02 19:19:28', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(4, 1, '2018-09-02 19:19:28', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(5, 1, '2018-09-02 19:19:28', 1, 'Truncando tabela temporária Webprice.'),
	(6, 2, '2018-09-02 19:19:28', 1, 'Baixando XML da Webprice.'),
	(7, 2, '2018-09-02 19:19:31', 1, 'Verificando download do XML da Webprice.'),
	(8, 2, '2018-09-02 19:19:31', 1, 'Erro ao baixar XML da Webprice.'),
	(9, 1, '2018-09-02 19:19:31', 1, 'Fim do processo.'),
	(10, 1, '2018-09-02 19:19:49', 1, 'Iniciando processo de precificação.'),
	(11, 1, '2018-09-02 19:19:49', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(12, 1, '2018-09-02 19:19:49', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(13, 1, '2018-09-02 19:19:49', 1, 'Truncando tabela temporária Webprice.'),
	(14, 2, '2018-09-02 19:19:49', 1, 'Baixando XML da Webprice.'),
	(15, 2, '2018-09-02 19:19:52', 1, 'Verificando download do XML da Webprice.'),
	(16, 2, '2018-09-02 19:19:52', 1, 'Erro ao baixar XML da Webprice.'),
	(17, 1, '2018-09-02 19:19:52', 1, 'Fim do processo.'),
	(18, 1, '2018-09-02 19:24:32', 1, 'Iniciando processo de precificação.'),
	(19, 1, '2018-09-02 19:24:32', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(20, 1, '2018-09-02 19:24:32', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(21, 1, '2018-09-02 19:24:32', 1, 'Truncando tabela temporária Webprice.'),
	(22, 2, '2018-09-02 19:24:32', 1, 'Baixando XML da Webprice.'),
	(23, 2, '2018-09-02 19:24:35', 1, 'Verificando download do XML da Webprice.'),
	(24, 2, '2018-09-02 19:24:35', 1, 'Erro ao baixar XML da Webprice.'),
	(25, 1, '2018-09-02 19:24:35', 1, 'Fim do processo.'),
	(26, 1, '2018-09-02 19:26:29', 1, 'Iniciando processo de precificação.'),
	(27, 1, '2018-09-02 19:26:54', 1, 'Iniciando processo de precificação.'),
	(28, 1, '2018-09-02 19:28:32', 1, 'Iniciando processo de precificação.'),
	(29, 1, '2018-09-02 19:28:32', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(30, 1, '2018-09-02 19:28:32', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(31, 1, '2018-09-02 19:28:32', 1, 'Truncando tabela temporária Webprice.'),
	(32, 2, '2018-09-02 19:28:32', 1, 'Baixando XML da Webprice.'),
	(33, 2, '2018-09-02 19:28:35', 1, 'Verificando download do XML da Webprice.'),
	(34, 2, '2018-09-02 19:28:35', 1, 'Erro ao baixar XML da Webprice.'),
	(35, 1, '2018-09-02 19:28:35', 1, 'Fim do processo.'),
	(36, 1, '2018-09-02 19:28:38', 1, 'Iniciando processo de precificação.'),
	(37, 1, '2018-09-02 19:28:38', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(38, 1, '2018-09-02 19:28:38', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(39, 1, '2018-09-02 19:28:38', 1, 'Truncando tabela temporária Webprice.'),
	(40, 2, '2018-09-02 19:28:38', 1, 'Baixando XML da Webprice.'),
	(41, 2, '2018-09-02 19:28:41', 1, 'Verificando download do XML da Webprice.'),
	(42, 2, '2018-09-02 19:28:41', 1, 'Erro ao baixar XML da Webprice.'),
	(43, 1, '2018-09-02 19:28:41', 1, 'Fim do processo.'),
	(44, 1, '2018-09-02 19:29:06', 1, 'Iniciando processo de precificação.'),
	(45, 1, '2018-09-02 19:29:58', 1, 'Iniciando processo de precificação.'),
	(46, 1, '2018-09-02 19:29:58', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(47, 1, '2018-09-02 19:29:58', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(48, 1, '2018-09-02 19:29:58', 1, 'Truncando tabela temporária Webprice.'),
	(49, 2, '2018-09-02 19:29:58', 1, 'Baixando XML da Webprice.'),
	(50, 2, '2018-09-02 19:30:02', 1, 'Verificando download do XML da Webprice.'),
	(51, 2, '2018-09-02 19:30:02', 1, 'Erro ao baixar XML da Webprice.'),
	(52, 1, '2018-09-02 19:30:02', 1, 'Fim do processo.'),
	(53, 1, '2018-09-02 19:32:36', 1, 'Iniciando processo de precificação.'),
	(54, 1, '2018-09-02 19:32:36', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(55, 1, '2018-09-02 19:32:36', 1, 'Deletando arquivo CSV da Webprice. Status: '),
	(56, 1, '2018-09-02 19:32:36', 1, 'Truncando tabela temporária Webprice.'),
	(57, 2, '2018-09-02 19:32:36', 1, 'Baixando XML da Webprice.'),
	(58, 2, '2018-09-02 19:32:39', 1, 'Verificando download do XML da Webprice.'),
	(59, 2, '2018-09-02 19:32:39', 1, 'Erro ao baixar XML da Webprice.'),
	(60, 1, '2018-09-02 19:32:39', 1, 'Fim do processo.'),
	(61, 1, '2018-09-02 19:34:09', 1, 'Iniciando processo de precificação.'),
	(62, 1, '2018-09-02 19:34:36', 1, 'Iniciando processo de precificação.'),
	(63, 1, '2018-09-02 19:34:36', 1, 'Deletando arquivo XML da Webprice. Status: '),
	(64, 1, '2018-09-02 19:34:36', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(65, 1, '2018-09-02 19:34:36', 1, 'Truncando tabela temporária Webprice.'),
	(66, 2, '2018-09-02 19:34:36', 1, 'Baixando XML da Webprice.'),
	(67, 2, '2018-09-02 19:34:43', 1, 'Verificando download do XML da Webprice.'),
	(68, 2, '2018-09-02 19:34:43', 1, 'XML Baixado com sucesso.'),
	(69, 2, '2018-09-02 19:34:43', 1, 'Gerando base webprice CSV'),
	(70, 2, '2018-09-02 19:34:44', 1, 'Importando CSV no banco.'),
	(71, 1, '2018-09-02 19:38:03', 1, 'Iniciando processo de precificação.'),
	(72, 1, '2018-09-02 19:38:03', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(73, 1, '2018-09-02 19:38:03', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(74, 1, '2018-09-02 19:38:03', 1, 'Truncando tabela temporária Webprice.'),
	(75, 2, '2018-09-02 19:38:03', 1, 'Baixando XML da Webprice.'),
	(76, 2, '2018-09-02 19:38:10', 1, 'Verificando download do XML da Webprice.'),
	(77, 2, '2018-09-02 19:38:10', 1, 'XML Baixado com sucesso.'),
	(78, 2, '2018-09-02 19:38:10', 1, 'Gerando base webprice CSV'),
	(79, 2, '2018-09-02 19:38:10', 1, 'Importando CSV no banco.'),
	(80, 1, '2018-09-02 19:40:04', 1, 'Iniciando processo de precificação.'),
	(81, 1, '2018-09-02 19:40:04', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(82, 1, '2018-09-02 19:40:04', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(83, 1, '2018-09-02 19:40:04', 1, 'Truncando tabela temporária Webprice.'),
	(84, 2, '2018-09-02 19:40:04', 1, 'Baixando XML da Webprice.'),
	(85, 2, '2018-09-02 19:40:12', 1, 'Verificando download do XML da Webprice.'),
	(86, 2, '2018-09-02 19:40:12', 1, 'XML Baixado com sucesso.'),
	(87, 2, '2018-09-02 19:40:12', 1, 'Gerando base webprice CSV'),
	(88, 2, '2018-09-02 19:40:12', 1, 'Importando CSV no banco.'),
	(89, 1, '2018-09-02 19:40:46', 1, 'Iniciando processo de precificação.'),
	(90, 1, '2018-09-02 19:40:46', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(91, 1, '2018-09-02 19:40:46', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(92, 1, '2018-09-02 19:40:46', 1, 'Truncando tabela temporária Webprice.'),
	(93, 2, '2018-09-02 19:40:46', 1, 'Baixando XML da Webprice.'),
	(94, 2, '2018-09-02 19:40:55', 1, 'Verificando download do XML da Webprice.'),
	(95, 2, '2018-09-02 19:40:55', 1, 'XML Baixado com sucesso.'),
	(96, 2, '2018-09-02 19:40:55', 1, 'Gerando base webprice CSV'),
	(97, 2, '2018-09-02 19:40:56', 1, 'Importando CSV no banco.'),
	(98, 1, '2018-09-02 19:41:36', 1, 'Iniciando processo de precificação.'),
	(99, 1, '2018-09-02 19:41:36', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(100, 1, '2018-09-02 19:41:36', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(101, 1, '2018-09-02 19:41:36', 1, 'Truncando tabela temporária Webprice.'),
	(102, 2, '2018-09-02 19:41:36', 1, 'Baixando XML da Webprice.'),
	(103, 2, '2018-09-02 19:41:42', 1, 'Verificando download do XML da Webprice.'),
	(104, 2, '2018-09-02 19:41:42', 1, 'XML Baixado com sucesso.'),
	(105, 2, '2018-09-02 19:41:42', 1, 'Gerando base webprice CSV'),
	(106, 2, '2018-09-02 19:41:43', 1, 'Importando CSV no banco.'),
	(107, 1, '2018-09-02 19:42:11', 1, 'Iniciando processo de precificação.'),
	(108, 1, '2018-09-02 19:42:11', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(109, 1, '2018-09-02 19:42:11', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(110, 1, '2018-09-02 19:42:11', 1, 'Truncando tabela temporária Webprice.'),
	(111, 2, '2018-09-02 19:42:11', 1, 'Baixando XML da Webprice.'),
	(112, 2, '2018-09-02 19:42:18', 1, 'Verificando download do XML da Webprice.'),
	(113, 2, '2018-09-02 19:42:18', 1, 'XML Baixado com sucesso.'),
	(114, 2, '2018-09-02 19:42:18', 1, 'Gerando base webprice CSV'),
	(115, 2, '2018-09-02 19:42:18', 1, 'Importando CSV no banco.'),
	(116, 1, '2018-09-02 19:43:42', 1, 'Iniciando processo de precificação.'),
	(117, 1, '2018-09-02 19:43:42', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(118, 1, '2018-09-02 19:43:42', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(119, 1, '2018-09-02 19:43:42', 1, 'Truncando tabela temporária Webprice.'),
	(120, 2, '2018-09-02 19:43:42', 1, 'Baixando XML da Webprice.'),
	(121, 2, '2018-09-02 19:43:51', 1, 'Verificando download do XML da Webprice.'),
	(122, 2, '2018-09-02 19:43:51', 1, 'XML Baixado com sucesso.'),
	(123, 2, '2018-09-02 19:43:51', 1, 'Gerando base webprice CSV'),
	(124, 2, '2018-09-02 19:43:52', 1, 'Importando CSV no banco.'),
	(125, 1, '2018-09-02 19:44:33', 1, 'Iniciando processo de precificação.'),
	(126, 1, '2018-09-02 19:44:33', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(127, 1, '2018-09-02 19:44:33', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(128, 1, '2018-09-02 19:44:33', 1, 'Truncando tabela temporária Webprice.'),
	(129, 2, '2018-09-02 19:44:33', 1, 'Baixando XML da Webprice.'),
	(130, 2, '2018-09-02 19:44:39', 1, 'Verificando download do XML da Webprice.'),
	(131, 2, '2018-09-02 19:44:39', 1, 'XML Baixado com sucesso.'),
	(132, 2, '2018-09-02 19:44:39', 1, 'Gerando base webprice CSV'),
	(133, 2, '2018-09-02 19:44:40', 1, 'Importando CSV no banco.'),
	(134, 1, '2018-09-02 19:47:44', 1, 'Iniciando processo de precificação.'),
	(135, 1, '2018-09-02 19:47:44', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(136, 1, '2018-09-02 19:47:44', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(137, 1, '2018-09-02 19:47:44', 1, 'Truncando tabela temporária Webprice.'),
	(138, 2, '2018-09-02 19:47:44', 1, 'Baixando XML da Webprice.'),
	(139, 2, '2018-09-02 19:47:51', 1, 'Verificando download do XML da Webprice.'),
	(140, 2, '2018-09-02 19:47:51', 1, 'XML Baixado com sucesso.'),
	(141, 2, '2018-09-02 19:47:51', 1, 'Gerando base webprice CSV'),
	(142, 2, '2018-09-02 19:47:53', 1, 'Importando CSV no banco.'),
	(143, 1, '2018-09-02 19:48:29', 1, 'Iniciando processo de precificação.'),
	(144, 1, '2018-09-02 19:48:29', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(145, 1, '2018-09-02 19:48:29', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(146, 1, '2018-09-02 19:48:29', 1, 'Truncando tabela temporária Webprice.'),
	(147, 2, '2018-09-02 19:48:29', 1, 'Baixando XML da Webprice.'),
	(148, 2, '2018-09-02 19:48:36', 1, 'Verificando download do XML da Webprice.'),
	(149, 2, '2018-09-02 19:48:36', 1, 'XML Baixado com sucesso.'),
	(150, 2, '2018-09-02 19:48:36', 1, 'Gerando base webprice CSV'),
	(151, 2, '2018-09-02 19:48:36', 1, 'Importando CSV no banco.'),
	(152, 1, '2018-09-02 19:49:48', 1, 'Iniciando processo de precificação.'),
	(153, 1, '2018-09-02 19:49:48', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(154, 1, '2018-09-02 19:49:48', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(155, 1, '2018-09-02 19:49:48', 1, 'Truncando tabela temporária Webprice.'),
	(156, 2, '2018-09-02 19:49:48', 1, 'Baixando XML da Webprice.'),
	(157, 2, '2018-09-02 19:49:54', 1, 'Verificando download do XML da Webprice.'),
	(158, 2, '2018-09-02 19:49:54', 1, 'XML Baixado com sucesso.'),
	(159, 2, '2018-09-02 19:49:54', 1, 'Gerando base webprice CSV'),
	(160, 2, '2018-09-02 19:49:55', 1, 'Importando CSV no banco.'),
	(161, 1, '2018-09-02 19:50:58', 1, 'Iniciando processo de precificação.'),
	(162, 1, '2018-09-02 19:50:58', 1, 'Deletando arquivo XML da Webprice. Status: 1'),
	(163, 1, '2018-09-02 19:50:58', 1, 'Deletando arquivo CSV da Webprice. Status: 1'),
	(164, 1, '2018-09-02 19:50:58', 1, 'Truncando tabela temporária Webprice.'),
	(165, 2, '2018-09-02 19:50:58', 1, 'Baixando XML da Webprice.'),
	(166, 2, '2018-09-02 19:51:05', 1, 'Verificando download do XML da Webprice.'),
	(167, 2, '2018-09-02 19:51:05', 1, 'XML Baixado com sucesso.'),
	(168, 2, '2018-09-02 19:51:05', 1, 'Gerando base webprice CSV'),
	(169, 2, '2018-09-02 19:51:06', 1, 'Importando CSV no banco.');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table pricing.marcas
CREATE TABLE IF NOT EXISTS `marcas` (
  `timestamp_changed` text,
  `user_changed` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `nome` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table pricing.marcas: 3 rows
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` (`timestamp_changed`, `user_changed`, `id`, `nome`) VALUES
	('2018-07-22 19:20:03', 2, 1, 'Brastemp'),
	('2018-07-22 19:24:33', 2, 2, 'Teste'),
	('2018-09-02 13:32:28', 2, 3, 'Bunda');
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;

-- Dumping structure for table pricing.params
CREATE TABLE IF NOT EXISTS `params` (
  `notification_emails` text,
  `webprice_xml_url` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webprice_json_url` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pricing.params: 1 rows
/*!40000 ALTER TABLE `params` DISABLE KEYS */;
INSERT INTO `params` (`notification_emails`, `webprice_xml_url`, `id`, `webprice_json_url`) VALUES
	('', 'http://doc.webglobal.com.br/xml_whirlpool.php', 1, '');
/*!40000 ALTER TABLE `params` ENABLE KEYS */;

-- Dumping structure for table pricing.user
CREATE TABLE IF NOT EXISTS `user` (
  `username` text,
  `password` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pricing.user: 2 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`username`, `password`, `id`, `name`) VALUES
	('system', 'system', 1, 'System'),
	('phboba', 'malcolm89', 2, 'Pedro Araujo');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table pricing.web_price
CREATE TABLE IF NOT EXISTS `web_price` (
  `sku` text,
  `id_webglobal` int(11) DEFAULT NULL,
  `preco_por` double DEFAULT NULL,
  `loja` text,
  `nome_produto` text,
  `meu_codigo` text,
  `market_place` text,
  `preco_avista` double DEFAULT NULL,
  `url_loja` text,
  `diferenca` double DEFAULT NULL,
  `menor_preco` double DEFAULT NULL,
  `maior_preco` double DEFAULT NULL,
  `meu_preco` double DEFAULT NULL,
  `disponibilidade` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table pricing.web_price: 0 rows
/*!40000 ALTER TABLE `web_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `web_price` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
