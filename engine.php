<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<?php

	/*
	Engine de Cálculo - Compra Certa - D2C
	Desenvolvida pela equipe de Produtividade e Performance/IPS do Compra Certa.
	 */

	header ('Content-type: text/html; charset=utf-8');
	set_time_limit(0);
	ob_implicit_flush(true);
	error_reporting(E_ALL & ~E_NOTICE);
	session_start();

	
	include __DIR__.'\php\connection.php'; //Info de conexão
	
	// -------------------- INICIALIZAÇÕES --------------------------

	//Usuário executor do processo
	if(isset($_SESSION['user_id'])){
		$usuario = $_SESSION['user_id'];
	}else{
		$usuario = 1;
	}
	
	$dataInicial = date('Y-m-d H:i:s');

	$db->insert('execution',['start' => $dataInicial,'hash' => md5($dataInicial),'user' => $usuario,'tipo' => 'pricing']);
	$execution_id = $db->id();
	logger($execution_id,'Iniciando processo de precificação. ID: '.$execution_id,1,$usuario,1);	
	
	
	
	
// -------------------- BASES --------------------------
	

	//Tratamento do arquivo da Webprice
	include __DIR__.'\php\engine\webprice.php';
	
	//Importa CSV Webprice no Banco
	logger($execution_id,'Importando CSV Webprice no banco.',2);
	$db->query("LOAD DATA LOCAL INFILE '".str_replace("\\", "/",  __DIR__)."/downloaded/webprice/webprice.csv' INTO TABLE pricing.web_price CHARACTER SET UTF8 FIELDS TERMINATED BY ';';");
	
	//Preenche base Webprice com o id da execução
	$db->update('web_price',['execution_id' => $execution_id,'timestamp'=>$dataInicial],['execution_id' => null]);
	


	//Trata ruptura na base da webprice
	//logger($execution_id,'Calculando SKUs em ruptura.',2);
	//$db->query("update web_price set ruptura = 0 where loja = market_place and execution_id = ".$execution_id.";");
	//$rupturas = $db->query("select sku, loja, market_place from web_price where ruptura = 0")->fetchAll(); ;

	 //foreach($rupturas as $ruptura){
     	//$db->update("web_price" , ["ruptura" => "0"] , ["sku" => $ruptura['sku'],"loja" => $ruptura['loja'],"execution_id" => $execution_id]);
	 //}

	
	//Preenchimento do estoque
	//include __DIR__.'\php\engine\estoque.php';

	
	//Importa CSV BAYES no Banco
	logger($execution_id,'Importando CSV Bayes no banco.',2);
	$db->query("LOAD DATA LOCAL INFILE '".str_replace("\\", "/",  __DIR__)."/downloaded/bayes/summary.csv' INTO TABLE pricing.bayes CHARACTER SET UTF8 FIELDS TERMINATED BY ';';");
	
	//Preenche base bayes com o id da execução
	$db->update('bayes',['execution_id' => $execution_id,'timestamp'=>$dataInicial],['execution_id' => null]);


	//Inicaliza base de preços sugeridos
	include __DIR__.'\php\engine\inicializar_tabela_preco_sugerido.php';

	


	//Inicializa preços VTEX
	include __DIR__.'\php\engine\nossos_precos.php';
	

	
// -------------------- CÁLCULOS --------------------------
	
	
	//Verifica Bayes
	//include __DIR__.'\php\engine\bayes.php';

	//Executa as regras
	include __DIR__.'\php\engine\regras.php';
	
	//Verifica Preços Manuais
	include __DIR__.'\php\engine\manual_prices.php';


	//Verifica Travas
	include __DIR__.'\php\engine\travas.php';
	
	//Verifica Preços Autorizados
	//include __DIR__.'\php\engine\authorized_prices.php';
	
	
	//Encerrando Execução
	$db->update('execution',['end' => date('Y-m-d H:i:s')],['id' => $execution_id]);
	logger($execution_id,'Fim do processo.',2);

	
?>

</body>
</html>