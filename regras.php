7 <?php 
	session_start();

	if(!isset($_SESSION['user_id'])){
		header('location:login.php');
	}
	
  include dirname(__FILE__)."\php\connection.php"; //Info de conexão

  if( isset($_GET['e']) && isset($_GET['id'])){
  	$resultSet = $db->query("
      SELECT 
      <sku.id>,
      <sku.sku>,
      <sku.modelo>,
      <sku.ambiente>,
      <sku.vtex_id>,
      <sku.usar_bayes>,
      <sku.ativo>,
      <sku.trava_minima>,
      <sku.trava_maxima>,
      <sku.segmento>,
      <sku.categoria>,
      <sku.subcategoria>,
      <sku.preco_tabela>
      FROM <sku>
      LEFT JOIN <ambientes> ON <ambientes.id> = <sku.ambiente>
  	  WHERE <sku.id> = " . $_GET['id']
    )->fetchAll();

  }else{
  	$resultSet = $db->query("
      SELECT 
      <regras.id>,
      <regras.ambienteTXT>,
      <regras.politicaTXT>,
      <regras.categoriaTXT>,
      <regras.SKU>,
      <regras.fator>,
      <regras.benchmarkTXT>
      FROM <regras>"
    )->fetchAll(); 

    $admins = $db->query("
      SELECT 
      <ambientes.id>,
      <ambientes.nome>
      FROM <ambientes>
      ")->fetchAll();

    $politicas = $db->query("
      SELECT 
      <politicas.id>,
      <politicas.nome>
      FROM <politicas>
      ")->fetchAll();

    $categorias = $db->query("
      SELECT 
      <sku.categoria>
      FROM <sku>
      GROUP BY 
      <sku.categoria>
      ")->fetchAll();

     $SKUs = $db->query("
      SELECT DISTINCT(<sku.sku>)
      FROM <sku>
      ")->fetchAll();

     $operandos = $db->query("
      SELECT 
      <regras_operandos.id>,
      <regras_operandos.descricao>
      FROM <regras_operandos>
      WHERE <tipo> = 'operando1'
      ")->fetchAll();

     $benchmarks = $db->query("
      SELECT 
      <regras_operandos.id>,
      <regras_operandos.descricao>
      FROM <regras_operandos>
      WHERE <tipo> = 'benchmark'
      ")->fetchAll();
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Portal de Pricing versão 3.0">
  <meta name="author" content="Produtividade e Perfomance D2C ">

  <title>Portal de Pricing D2C</title>

  <!-- Bootstrap core CSS
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> 
  <!-- Custom fonts for this template -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">



  

</head>

<body class="fixed-nav sticky-footer bg-dark " id="page-top">
  
<?php include "php/navigation.php";?>  

  
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">Regras</li>
      </ol>
      
      <!-- CONTEÚDO -->
      <div class="conteudo">
	            
	   
	  
	  <?php if( isset($_GET['e']) && isset($_GET['id'])): ?> 
      <?php foreach($resultSet as $linha): ?>
              
         
      <?php endforeach; ?>

    <?php else: ?> <!-- LISTAR -->

        <a href="#" class="nav-link" data-toggle="modal" data-target="#modal">
          <i class="fa fa-plus-circle" aria-hidden="true"></i> Nova regra
        </a> 

        <div class="text-center">
          <h2>Regras</h2><hr>
        </div>

        <div id="regras" class="text-center">
           
           <table class="table table-bordered nowrap" id="tabela" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Ambiente</th>
                <th>Política</th>
                <th>SKU</th>
                <th>Fator</th>
                <th>Benchmark</th>
              </tr>
            </thead>
            
            <tbody>
            <?php foreach($resultSet as $linha): ?>
              <tr>
        
              <td><a href="regras.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['ambienteTXT']; ?></a></td>
              <td><a href="regras.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['politicaTXT']; ?></a></td>
              <td><a href="regras.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['SKU']; ?></a></td>
              <td><a href="regras.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['fator']; ?></a></td>
              <td><a href="regras.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['benchmarkTXT']; ?></a></td>
        
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>


          
        </div>

		<?php endif; ?>
        
      </div>
      

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Desenvolvido por: Produtividade e Performance | Compra Certa | D2C </small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Modal Novo SKU -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="labelModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModal">Nova Regra</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body text-center">

            <form action="php/cadastrar.php" method="POST">

             Para o Admin <br>
            <select id="ambiente" name = "ambiente" style="width:120px;" required="required"><option value = ""></option>
              <?php foreach($admins as $admin): ?> <option value="<?php echo $admin['id'];?>"><?php echo $admin['nome'];?></option> <?php endforeach;?></select>

             <br><br>Política <br>

            <select id="politica" name = "politica" style="width:150px;" required="required"><option value = ""></option>
              <?php foreach($politicas as $politica): ?><option value="<?php echo $politica['id'];?>"><?php echo $politica['nome'];?></option><?php endforeach;?></select>

            <br><br>Categoria <br>

            <select style="width:180px;" id="categoria" name="categoria" required="required"> <option value = "Todas">Todas</option>
            </select>

            <br><br>SKU <br>

            <select id="SKU" name = "SKU" style="width:150px;" required="required"><option value = ""></option>
              <?php foreach($SKUs as $sku): ?>
              <option value="<?php echo $sku['sku'];?>"><?php echo $sku['sku'];?></option><?php endforeach;?>
            </select>

              <!--
              <br><br>SE <br>
              <select style="width:250px;" id="operando1" name = "operando1">
              <?php foreach($operandos as $operando): ?><option value = "<?php echo $operando['id'];?>"><?php echo $operando['descricao'];?></option><?php endforeach;?></select>
              -->
            <br><br>o preço sugerido será igual a  

            <input type="text" value="1" style="width:50px;" id="fator" name = "fator"> vezes 

            <select style="width:120px;" id="benchmark" name = "benchmark" required="required"><option value = ""></option><?php foreach($benchmarks as $benchmark): ?><option value = "<?php echo $benchmark['id'];?>"><?php echo $benchmark['descricao'];?></option><?php endforeach;?></select>
            <br> <br> <br>

            <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
            <input type="hidden" name="atualizar" value="0"/>
            <button name="submit" type="submit" class="btn btn-primary">Cadastrar</button>
          </form>
          
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Sucesso -->
    <div class="modal fade" id="modalSucesso" tabindex="-1" role="dialog" aria-labelledby="labelModalSucesso" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModalSucesso">Regra</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="false">×</span>
            </button>
          </div>
          <div class="modal-body">
            Regra salva com sucesso.        
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript -->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-charts.min.js"></script>

    <!-- ATIVA O MODAL DE SUCESSO NO CADASTRO -->
    <?php if(isset($_GET['s'])) : ?>
      <script type="text/javascript">
        $(document).ready(function(){
          $("#modalSucesso").modal('show');
      });
      </script>
    <?php endif; ?>

    <!-- Ativa a Tabela -->
    <script type="text/javascript">

      $(document).ready(function() {
        $('#tabela').DataTable({
          "scrollX": true
        });
      });
    </script>

  </div>
</body>

</html>
