<?php 

session_start();

	if(!isset($_SESSION['user_id'])){
		header('location:login.php');
	}
	
  include dirname(__FILE__)."\php\connection.php"; //Info de conexão

  if( isset($_GET['e']) && isset($_GET['id'])){
    $resultSet = $db->query("
      SELECT 
      <ambientes.id>,
      <ambientes.nome>,
      <ambientes.api_url>,
      <ambientes.vtex_key>,
      <ambientes.vtex_token>,
      <ambientes.timestamp_changed>,
      <user.name> as <userchanged>
      FROM <ambientes>
      LEFT JOIN <user> ON <ambientes.user_changed> = <user.id>
      WHERE <ambientes.id> = " . $_GET['id']
    )->fetchAll();
  }else{
    $resultSet = $db->query("
      SELECT 
      <ambientes.id>,
      <ambientes.nome>,
      <ambientes.api_url>,
      <ambientes.vtex_key>,
      <ambientes.vtex_token>,
      <ambientes.timestamp_changed>,
      <user.name> as <userchanged>
      FROM <ambientes>
      LEFT JOIN <user> ON <ambientes.user_changed> = <user.id>"
    )->fetchAll();    
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Portal de Pricing versão 3.0">
  <meta name="author" content="Produtividade e Perfomance D2C ">

  <title>Portal de Pricing D2C</title>

  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

  

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  
<?php include "php/navigation.php";?>  

  
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">Ambientes</li>
      </ol>
      
      <!-- CONTEÚDO -->
      <div class="conteudo">

        <!-- EDITAR -->
        <?php if( isset($_GET['e']) && isset($_GET['id'])): ?> 
          
          <form action="php/cadastrar.php" method="POST">
            <div class="form-group row">
              <label for="nome" class="col-3 col-form-label">Nome</label> 
              <div class="col-9">
                <input id="nome" name="nome" placeholder="Ex: Compra Certa" type="text" class="form-control here" required="required" value="<?php echo $resultSet[0]['nome']; ?>" >
              </div>
            </div>
            <div class="form-group row">
              <label for="api_url" class="col-3 col-form-label">URL da API</label> 
              <div class="col-9">
                <input id="api_url" name="api_url" placeholder="Ex: http://compracerta.vtexcommercestable.com.br" type="text" class="form-control here" required="required" value="<?php echo $resultSet[0]['api_url']; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="vtex_key" class="col-3 col-form-label">X-VTEX-API-AppKey</label> 
              <div class="col-9">
                <input id="vtex_key" name="vtex_key" placeholder="Ex: vtexappkey-compracerta-PGLJLX" type="text" class="form-control here" required="required" value="<?php echo $resultSet[0]['vtex_key']; ?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="vtex_token" class="col-3 col-form-label">X-VTEX-API-AppToken</label> 
              <div class="col-9">
                <input id="vtex_token" name="vtex_token" placeholder="Ex: SNWGYYVWLKATTEEQDANNHSIZSAPDHNAARBICXTSOWPGMXWVUCXOZQWUAEWOXISOXJABRVBTJIYQSLWBVFUBJZBKVHIYAFVGEAJDXUDYADFRSFBOIUSSNQEXIPGJNDTDN" type="text" class="form-control here" value="<?php echo $resultSet[0]['vtex_token']; ?>">
              </div>
            </div> 
            <div class="form-group row">
              <div class="offset-5 col-9">
                <button name="submit" type="submit" class="btn btn-primary">Atualizar</button>
              </div>
            </div>
            <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
            <input type="hidden" name="ambiente_id" value="<?php echo $_GET['id']; ?>"/>
			<input type="hidden" name="atualizar" value="1"/>
          </form>

        <?php else: ?> <!-- LISTAR -->
        
          <a href="#" class="nav-link" data-toggle="modal" data-target="#modal">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> Novo Ambiente
          </a>
          
          <table class="table table-bordered nowrap" id="tabela" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>URL</th>
                <th>VTEX Key</th>
                <th>VTEX Token</th>
                <th>Changed By</th>
                <th>Change Date</th>
              </tr>
            </thead>
            
            <tbody>
            <?php foreach($resultSet as $linha): ?>
              <tr>
			  
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['id']; ?></a></td>
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['nome']; ?></a></td>
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['api_url']; ?></a></td>
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['vtex_key']; ?></a></td>
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['vtex_token']; ?></a></td>
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['userchanged']; ?></a></td>
              <td><a href="ambientes.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['timestamp_changed']; ?></a></td>
			  
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>

        <?php endif; ?>
        
      </div>
      

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Desenvolvido por: Produtividade e Performance | Compra Certa | D2C </small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>


    <!-- Modal Novo Cadastro -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="labelModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModal">Novo Ambiente</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">

            <form action="php/cadastrar.php" method="POST">
            <div class="form-group row">
              <label for="nome" class="col-3 col-form-label">Nome</label> 
              <div class="col-9">
                <input id="nome" name="nome" placeholder="Ex: Compra Certa" type="text" class="form-control here" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="api_url" class="col-3 col-form-label">URL da API</label> 
              <div class="col-9">
                <input id="api_url" name="api_url" placeholder="Ex: http://compracerta.vtexcommercestable.com.br" type="text" class="form-control here" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="vtex_key" class="col-3 col-form-label">X-VTEX-API-AppKey</label> 
              <div class="col-9">
                <input id="vtex_key" name="vtex_key" placeholder="Ex: vtexappkey-compracerta-PGLJLX" type="text" class="form-control here" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="vtex_token" class="col-3 col-form-label">X-VTEX-API-AppToken</label> 
              <div class="col-9">
                <input id="vtex_token" name="vtex_token" placeholder="Ex: SNWGYYVWLKATTEEQDANNHSIZSAPDHNAARBICXTSOWPGMXWVUCXOZQWUAEWOXISOXJABRVBTJIYQSLWBVFUBJZBKVHIYAFVGEAJDXUDYADFRSFBOIUSSNQEXIPGJNDTDN" type="text" class="form-control here">
              </div>
            </div> 
            <div class="form-group row">
              <div class="offset-5 col-9">
                <button name="submit" type="submit" class="btn btn-primary">Cadastrar</button>
              </div>
            </div>
            <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
          </form>
          
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Sucesso -->
    <div class="modal fade" id="modalSucesso" tabindex="-1" role="dialog" aria-labelledby="labelModalSucesso" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModalSucesso">Novo ambiente</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="false">×</span>
            </button>
          </div>
          <div class="modal-body">
            Cadastro efetuado com sucesso!          
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript -->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-charts.min.js"></script>

    <!-- ATIVA O MODAL DE SUCESSO NO CADASTRO -->
    <?php if(isset($_GET['s'])) : ?>
      <script type="text/javascript">
        $(document).ready(function(){
          $("#modalSucesso").modal('show');
      });
      </script>
    <?php endif; ?>

    <!-- Ativa a Tabela -->
    <script type="text/javascript">

      $(document).ready(function() {
        $('#tabela').DataTable({
          "scrollX": true
        });
      });
    </script>

  </div>
</body>

</html>
