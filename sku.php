<?php 
	session_start();

	if(!isset($_SESSION['user_id'])){
		header('location:login.php');
	}
	
  include dirname(__FILE__)."\php\connection.php"; //Info de conexão

  if( isset($_GET['e']) && isset($_GET['id'])){
  	$resultSet = $db->query("
      SELECT 
      <sku.id>,
      <sku.sku>,
      <sku.modelo>,
      <sku.ambiente>,
      <sku.vtex_id>,
      <sku.usar_bayes>,
      <sku.ativo>,
      <sku.trava_minima>,
      <sku.trava_maxima>,
      <sku.segmento>,
      <sku.categoria>,
      <sku.subcategoria>,
      <sku.preco_tabela>
      FROM <sku>
      LEFT JOIN <ambientes> ON <ambientes.id> = <sku.ambiente>
  	  WHERE <sku.id> = " . $_GET['id']
    )->fetchAll();

    $ambientes = $db->query("
      SELECT 
      <ambientes.id>,
      <ambientes.nome>
      FROM <ambientes>"
    )->fetchAll();
  }else{
  	$resultSet = $db->query("
      SELECT 
      <sku.id>,
      <ambientes.nome> as <ambiente>,
      <sku.sku>,
      <sku.modelo>,
      <sku.segmento>,
      <sku.categoria>
      FROM <sku>
      LEFT JOIN <ambientes> ON <ambientes.id> = <sku.ambiente>"
    )->fetchAll(); 
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Portal de Pricing versão 3.0">
  <meta name="author" content="Produtividade e Perfomance D2C ">

  <title>Portal de Pricing D2C</title>

  <!-- Bootstrap core CSS
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> 
  <!-- Custom fonts for this template -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">



  

</head>

<body class="fixed-nav sticky-footer bg-dark " id="page-top">
  
<?php include "php/navigation.php";?>  

  
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">SKUs</li>
      </ol>
      
      <!-- CONTEÚDO -->
      <div class="conteudo">
	            
	   
	  
	  <?php if( isset($_GET['e']) && isset($_GET['id'])): ?> 
          <?php foreach($resultSet as $linha): ?>
              
          <form action="php/cadastrar.php" method="POST">
           <div class="form-group row">
            <label class="col-2">Status do SKU</label> 
            <div class="col-4">
                <input name="ativo" type="radio" value="1" <?php echo ($linha['ativo'])?'checked':'' ?> > Ativo
                <input name="ativo" type="radio" value="0" <?php echo ($linha['ativo'])?'':'checked' ?> > Inativo
            </div>

            <label class="col-2">Usar Preço Bayes</label> 
            <div class="col-4">
                <input name="bayes" type="radio" value="1" <?php echo ($linha['usar_bayes'])?'checked':'' ?> > Sim
                <input name="bayes" type="radio" value="0" <?php echo ($linha['usar_bayes'])?'':'checked' ?> > Não
            </div>

          </div>
          <div class="form-group row">
            <label for="sku" class="col-2 col-form-label">SKU</label> 
            <div class="col-2">
              <input id="sku" name="sku" type="text" class="form-control here" value="<?php echo $linha['sku']; ?>">
            </div>
          
            <label for="modelo" class="col-2 col-form-label" >Modelo</label> 
            <div class="col-2">
              <input id="modelo" name="modelo" type="text" class="form-control here" value="<?php echo $linha['modelo']; ?>">
            </div>
            <label for="ambiente" class="col-2 col-form-label">Ambiente</label> 
            <div class="col-2">
              <select id="ambiente" name="ambiente" class="custom-select" required="required">
                <?php foreach($ambientes as $ambiente): ?>
                  <option value="<?php echo $ambiente['id']; ?>"><?php echo $ambiente['nome']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="segmento" class="col-2 col-form-label">Segmento</label> 
            <div class="col-2">
              <input id="segmento" name="segmento" type="text" class="form-control here" required="required" value="<?php echo $linha['segmento']; ?>">
            </div>
            <label for="categoria" class="col-2 col-form-label">Categoria</label> 
            <div class="col-2">
              <input id="categoria" name="categoria" type="text" class="form-control here" required="required" value="<?php echo $linha['categoria']; ?>">
            </div>
            <label for="subcategoria" class="col-2 col-form-label">Sub-categoria</label> 
            <div class="col-2">
              <input id="subcategoria" name="subcategoria" type="text" class="form-control here" required="required" value="<?php echo $linha['subcategoria']; ?>">
            </div>
          </div>
          
          <div class="form-group row">
            <label for="trava_minima" class="col-2 col-form-label">Trava Mínima</label> 
            <div class="col-2">
              <input id="trava_minima" name="trava_minima" type="text" class="form-control here" required="required" value="<?php echo $linha['trava_minima']; ?>">
            </div>
            <label for="trava_maxima" class="col-2 col-form-label">Trava Máxima</label> 
            <div class="col-2">
              <input id="trava_maxima" name="trava_maxima" type="text" class="form-control here" required="required" value="<?php echo $linha['trava_maxima']; ?>">
            </div>
            <label for="preco_tabela" class="col-2 col-form-label">Preço Tabela</label> 
            <div class="col-2">
              <input id="preco_tabela" name="preco_tabela" type="text" class="form-control here" required="required" value="<?php echo $linha['preco_tabela']; ?>">
            </div>
          </div> 
         <div class="form-group row">
            <label for="vtex_id" class="col-2 col-form-label">VTEX ID</label> 
            <div class="col-2">
              <input id="vtex_id" name="vtex_id" type="text" class="form-control here" required="required" value="<?php echo $linha['vtex_id']; ?>">
            </div>
          </div>
          
          <div class="form-group row">
            <div class="offset-5 col-12">
              <button name="submit" type="submit" class="btn btn-primary">Salvar</button>
            </div>
          </div>
          <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
          <input type="hidden" name="sku_id" value="<?php echo $_GET['id']; ?>"/>
          <input type="hidden" name="atualizar" value="1"/>
        </form>
      <?php endforeach; ?>

    <?php else: ?> <!-- LISTAR -->

      <!--
        <a href="#" class="nav-link" data-toggle="modal" data-target="#modal">
          <i class="fa fa-plus-circle" aria-hidden="true"></i> Novo SKU 
        </a> 
       --> 
        <table class="table table-bordered nowrap" id="tabela" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>SKU</th>
              <th>Modelo</th>
              <th>Segmento</th>
              <th>Categoria</th>
              <th>Ambiente</th>
            </tr>
          </thead>
          
          <tbody>
          <?php foreach($resultSet as $linha): ?>
            <tr>
              <td><a href="sku.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['sku']; ?></a></td>
              <td><a href="sku.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['modelo']; ?></a></td>
              <td><a href="sku.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['segmento']; ?></a></td>
              <td><a href="sku.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['categoria']; ?></a></td>
              <td><a href="sku.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['ambiente']; ?></a></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
		<?php endif; ?>
        
      </div>
      

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Desenvolvido por: Produtividade e Performance | Compra Certa | D2C </small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Modal Novo SKU -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="labelModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModal">Nova Marca</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">

            <form action="php/cadastrar.php" method="POST">

            <div class="form-group row">
              <label for="nome" class="col-3 col-form-label">Nome</label> 
              <div class="col-9">
                <input id="nome" name="nome" placeholder="Ex: Brastemp" type="text" class="form-control here" required="required">
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-5 col-9">
                <button name="submit" type="submit" class="btn btn-primary">Cadastrar</button>
              </div>
            </div>
            <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
          </form>
          
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Sucesso -->
    <div class="modal fade" id="modalSucesso" tabindex="-1" role="dialog" aria-labelledby="labelModalSucesso" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="labelModalSucesso">SKU</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="false">×</span>
            </button>
          </div>
          <div class="modal-body">
            Cadastro salvo com sucesso!          
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript -->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-charts.min.js"></script>

    <!-- ATIVA O MODAL DE SUCESSO NO CADASTRO -->
    <?php if(isset($_GET['s'])) : ?>
      <script type="text/javascript">
        $(document).ready(function(){
          $("#modalSucesso").modal('show');
      });
      </script>
    <?php endif; ?>

    <!-- Ativa a Tabela -->
    <script type="text/javascript">

      $(document).ready(function() {
        $('#tabela').DataTable({
          "scrollX": true
        });
      });
    </script>

  </div>
</body>

</html>
