<?php 
	session_start();

	if(!isset($_SESSION['user_id'])){
		header('location:login.php');
	}

  include dirname(__FILE__)."\php\connection.php"; //Info de conexão
  require_once 'php/vendor/medoo/Medoo.php'; //Biblioteca para tratar das requisições ao Banco

  use Medoo\Medoo;

  $nroPrecificados = $db->query("SELECT count(preco_sugerido.precificado) FROM <preco_sugerido> WHERE <preco_sugerido.precificado> = 1 and <preco_sugerido.execution_id> = (select max(<execution.id>) from <execution>)")->fetchAll();

  $nroNaoPrecificados = $db->query("SELECT count(preco_sugerido.precificado) FROM <preco_sugerido> WHERE <preco_sugerido.precificado> = 0 and <preco_sugerido.execution_id> = (select max(<execution.id>) from <execution>)")->fetchAll();

  $horarios = $db->query("SELECT execution.start, execution.end from execution order by id desc limit 1")->fetchAll();
  $intervalo = (strtotime($horarios[0][1]) - strtotime($horarios[0][0]))/60;




  $resultSet = $db->query("
    SELECT 
    <preco_sugerido.sku>,
    <politicas.nome>,
    <preco_sugerido.preco>,
    <regras.fator>,
    <preco_sugerido.log_msg>,
    <preco_sugerido.buybox>,
    <preco_sugerido.varejista>,
    <preco_sugerido.trava_minima>,
    <preco_sugerido.trava_maxima>,
    <preco_sugerido.preco_tabela>,
    <preco_sugerido.preco_site_aberto>,
    <preco_sugerido.maior_preco>,
    <preco_sugerido.menor_preco>
    FROM <preco_sugerido>
    left join <regras> on
    <preco_sugerido.ambiente> = <regras.ambiente> and 
    <preco_sugerido.politica> = <regras.politica> and
    <preco_sugerido.sku> = <regras.SKU>
    left join <politicas> on
    <politicas.id> = <preco_sugerido.politica>
    WHERE <preco_sugerido.precificado> = 1
    and <preco_sugerido.execution_id> = (select max(<execution.id>) from <execution>)"
  )->fetchAll(); 
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Portal de Pricing versão 3.0">
  <meta name="author" content="Produtividade e Perfomance D2C ">

  <title>Portal de Pricing D2C</title>

  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  
<?php include "php/navigation.php";?>  

  <!-- CONTEÚDO -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>

      <!-- Cartões -->
      <div class="row">
        <div class="col-md-3">
          <div class="card" >
            <div class="card-header tex-center">
              <div class="text-center"><h1><?php echo $nroPrecificados[0][0];?></h1></div>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item text-center">SKUs precificados</li>
              <li class="list-group-item text-center"><b><?php echo $nroNaoPrecificados[0][0];?></b> SKUs não precificados</li>
            </ul>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card" >
            <div class="card-header tex-center">
              <div class="text-center"><img src="php/graficos/index-pizza.php" border=0 alt="BOS Regras Aplicadas" align="center"></div>
            </div>
          </div>

          

        </div>
        <div class="col-md-3">
          <?php if($intervalo < 60) : ?>
          <div class="card" >
            <div class="card-header tex-center">
              <div class="text-center"><h1><?php echo ($intervalo*60);?></h1></div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item text-center">segundos (última execução)</li>
                <li class="list-group-item text-center"><b>Início</b>: <?php echo $horarios[0][0];?><br><b>Fim</b>: <?php echo $horarios[0][1];?></li>
            </ul>
          </div>
          <?php else : ?>
          <div class="card" >
          <div class="card-header tex-center">
            <div class="text-center"><h1><?php echo ($intervalo);?></h1></div>
          </div>
          <ul class="list-group list-group-flush">
              <li class="list-group-item text-center">minutos (última execução)</li>
              <li class="list-group-item text-center"><b>Início</b>: <?php echo $horarios[0][0];?><br><b>Fim</b>: <?php echo $horarios[0][1];?></li>
          </ul>
        </div>
          <?php endif; ?>



        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12 text-center"><h3>&nbsp;Log da Última Precificação</h3></div>
         <table class="table table-bordered nowrap" id="tabela" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Política</th>
              <th>SKU</th>
              <th>Preço Sugerido</th>
              <th>Fator</th>
              <th>Trava Mínima</th>
              <th>Trava Máxima</th>
              <th>BuyBox</th>
              <th>Varejista</th>
              <th>Tabela</th>
              <th>Site Aberto</th>
              <th>Maior</th>
              <th>Menor</th>
              <th>Log</th>
            </tr>
          </thead>
          
          <tbody>
          <?php foreach($resultSet as $linha): ?>
            <tr>
              <td><?php echo $linha['nome']; ?></td>
              <td><?php echo $linha['sku']; ?></td>
              <td><?php echo $linha['preco']; ?></td>
              <td><?php echo $linha['fator']; ?></td>
              <td><?php echo $linha['trava_minima']; ?></td>
              <td><?php echo $linha['trava_maxima']; ?></td>
              <td><?php echo $linha['buybox']; ?></td>
              <td><?php echo $linha['varejista']; ?></td>
              <td><?php echo $linha['preco_tabela']; ?></td>
              <td><?php echo $linha['preco_site_aberto']; ?></td>
              <td><?php echo $linha['maior_preco']; ?></td>
              <td><?php echo $linha['menor_preco']; ?></td>
              <td><?php echo $linha['log_msg']; ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>


    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Desenvolvido por: Produtividade e Performance | Compra Certa | D2C </small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>



    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript -->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-charts.min.js"></script>
    <!-- Ativa a Tabela -->
    <script type="text/javascript">

      $(document).ready(function() {
        $('#tabela').DataTable({
          "scrollX": true
        });
      });
    </script>

  </div>
</body>

</html>
