<!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.php">Portal de Pricing D2C</a>

    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">

      <!-- MENU LATERAL -->
      <ul class="navbar-nav navbar-sidenav" id="MenuLateral">

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="index.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Cadastros">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseCadastros" data-parent="#MenuLateral">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            <span class="nav-link-text">Cadastros</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseCadastros">
            <li>
              <a href="ambientes.php">Ambientes</a>
            </li>
            <li>
              <a href="marcas.php">Marcas</a>
            </li>
            <li>
              <a href="politicas.php">Políticas Comerciais</a>
            </li>
            <li>
              <a href="sku.php">Tabela de SKUs</a>
            </li>
            <li>
              <a href="regras.php">Regras</a>
            </li>
			       <li>
              <a href="bayes.php">Bayes</a>
            </li>
			<!--
            <li>
              <a href="utmi.php">UTMI</a>
            </li>
            <li>
              <a href="clusters.php">Clusters</a>
            </li> -->
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Scheduler">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseScheduler" data-parent="#MenuLateral">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span class="nav-link-text">Execução</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseScheduler">
            <!--<li>
              <a href="execucaoAutomatica.php">Execução Automática</a>
            </li>-->
            <li>
              <a href="execucaoManual.php">Execução Manual</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Parâmetros">
          <a class="nav-link" href="parametros.php">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="nav-link-text">Parâmetros</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Alavancas">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseAlavancas" data-parent="#MenuLateral">
            <i class="fa fa-money" aria-hidden="true"></i>
            <span class="nav-link-text">Alavancas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseAlavancas">
            <li>
              <a href="#">Promoções Permanentes</a>
            </li>
            <li>
              <a href="#">Promoções Manuais</a>
            </li>
            <li>
              <a href="a_execucaoManual.php">Execução Manual</a>
            </li>
          </ul>
        </li>

      


      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>

	  
	  
      <!-- MENU SUPERIOR -->
      <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="color:white">
          <i class="fa fa-user"></i> <?php if(isset($_SESSION['nome'])){echo $_SESSION['nome']." | ";} ?>
          <a href="php/logout.php"><i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
	  


    </div>
  </nav>