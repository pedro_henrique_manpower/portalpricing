<?php

	session_start();
	include dirname(__FILE__)."/connection.php"; //Info de conexão
	
	switch ($_POST['tipoDeCadastro']){

		case "ambientes.php": 
		 	
			
			if($_POST['atualizar'] == "1"){
				$db->update("ambientes", [
					'nome' 			=> 	$_POST['nome'],
					'api_url' 		=> 	$_POST['api_url'],
					'vtex_key' 		=> 	$_POST['vtex_key'],
					'vtex_token' 	=> 	$_POST['vtex_token'],
					'user_changed' 	=> 	$_SESSION['user_id'] 
				],[	
					'id' 			=> 	$_POST['ambiente_id']
				]);
			}else{
				$db->insert("ambientes", [
					'nome' 			=> 	$_POST['nome'],
					'api_url' 		=> 	$_POST['api_url'],
					'vtex_key' 		=> 	$_POST['vtex_key'],
					'vtex_token' 	=> 	$_POST['vtex_token'],
					'user_changed' 	=> 	$_SESSION['user_id']
				]);
			}
			
			header("Location: ../ambientes.php?s=1");
			break;

		case "marcas.php":
		
			if($_POST['atualizar'] == "1"){
				$db->update("marcas", [
					'nome' 			=> 	$_POST['nome'],
					'user_changed' 	=> 	$_SESSION['user_id']
				],[	
					'id' 			=> 	$_POST['marca_id']
				]);
			}else{
				$db->insert("marcas", [
					'nome' 			=> 	$_POST['nome'],
					'user_changed' 	=> 	$_SESSION['user_id']
				]);
			}
			header("Location: ../marcas.php?s=1");
			break;

		case "sku.php":
		
			if($_POST['atualizar'] == "1"){

				$bayes = intval($_POST['bayes']);
				$ativo = intval($_POST['ativo']);

				$db->update("sku", [
					'sku' 			=> 	$_POST['sku'],
					'modelo' 		=> 	$_POST['modelo'],
					'ambiente' 		=> 	$_POST['ambiente'],
					'vtex_id' 		=> 	$_POST['vtex_id'],
					'usar_bayes' 	=> 	$bayes,
					'ativo' 		=> 	$ativo,
					'trava_minima' 	=>  $_POST['trava_minima'],
					'trava_maxima' 	=> 	$_POST['trava_maxima'],
					'segmento' 		=>  $_POST['segmento'],
					'categoria' 	=> 	$_POST['categoria'],
					'subcategoria' 	=> 	$_POST['subcategoria'],
					'user_changed' 	=> 	$_SESSION['user_id'],
					'preco_tabela' 	=> 	$_POST['preco_tabela']
				],[	
					'id' 			=> 	$_POST['sku_id']
				]);

			}else{
				//$db->insert("sku", [
				//	'nome' 			=> 	$_POST['nome'],
			//		'user_changed' 	=> 	$_SESSION['user_id']
				//]);
			}
			header("Location: ../sku.php?s=1&e=1&id=".$_POST['sku_id']);
			break;

		case "politicas.php":

			if($_POST['atualizar'] == "1"){

				$db->update("politicas", [
					'nome' 			=> 	$_POST['nome_politica'],
					'loja_webprice' => 	$_POST['loja_webprice'],
					'ambiente' 		=> 	$_POST['ambiente'],
					'saleschannel'	=> 	$_POST['vtex_saleschannel'],
					'user_changed' 	=> 	$_SESSION['user_id']
				],[	
					'id' 			=> 	$_POST['politica_id']
				]);
				
				header("Location: ../politicas.php?s=1&e=1&id=".$_POST['politica_id']);
				break;

			}else{
				$db->insert("politicas", [
					'nome' 			=> 	$_POST['nome_politica'],
					'ambiente' 		=> 	$_POST['ambiente'],
					'loja_webprice' => 	$_POST['loja_webprice'],
					'saleschannel'	=> 	$_POST['vtex_saleschannel'],
					'user_changed' 	=> 	$_SESSION['user_id']
				]);
			}

			header("Location: ../politicas.php?s=1");
			break;

			case "regras.php":

				if($_POST['atualizar'] == "1"){

					$db->update("politicas", [
						'nome' 			=> 	$_POST['nome_politica'],
						'ambiente' 		=> 	$_POST['ambiente'],
						'saleschannel'	=> 	$_POST['vtex_saleschannel'],
						'user_changed' 	=> 	$_SESSION['user_id']
					],[	
						'id' 			=> 	$_POST['politica_id']
					]);
					
					header("Location: ../politicas.php?s=1&e=1&id=".$_POST['politica_id']);
					break;

				}else{
					$db->insert("regras", [
						'ativo'			=> 	'1',
						'ambiente' 		=> 	$_POST['ambiente'],
						'politica' 		=> 	$_POST['politica'],
						'SKU' 			=> 	$_POST['SKU'],
						'categoriaTXT'	=> 	$_POST['categoria'],
						'operando1'		=> 	0,
						'fator'			=> 	$_POST['fator'],
						'benchmark'		=> 	$_POST['benchmark'],
						'user_changed' 	=> 	$_SESSION['user_id']
					]);

					//Insere as descrições
					$idRegra 	=  $db->id();
					$idAmbiente =  $db->get("regras","ambiente",["id" => $idRegra]);
					$idPolitica =  $db->get("regras","politica",["id" => $idRegra]);
					$idOperando1 = $db->get("regras","operando1",["id" => $idRegra]);
					$idBenchmark = $db->get("regras","benchmark",["id" => $idRegra]);
					
					$ambienteTXT = $db->get("ambientes","nome",["id" => $idAmbiente]);
					$politicaTXT = $db->get("politicas","nome",["id" => $idPolitica]);
					$operandoTXT = $db->get("regras_operandos","descricao",["id" => $idOperando1]);
					$benchmarkTXT =$db->get("regras_operandos","descricao",["id" => $idBenchmark]);

					//echo $idRegra ."<br>".$idAmbiente ."<br>".$idPolitica."<br>".$idOperando1 ."<br>".$idBenchmark ."<br>".$ambienteTXT."<br>".$politicaTXT ."<br>".$operandoTXT ."<br>".$benchmarkTXT;

					$db->update("regras",
						[
						"ambienteTXT"=>$ambienteTXT,
						"politicaTXT"=>$politicaTXT,
						"operando1TXT"=>$operandoTXT,
						"benchmarkTXT"=>$benchmarkTXT
					],[
						"id"=>$idRegra 
					]);
				}

				header("Location: ../regras.php?s=1");
				break;
	}


?>