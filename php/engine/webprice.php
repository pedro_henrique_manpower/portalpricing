<?php

	//Limpa arquivos
	$result = unlink ( __DIR__.'\..\..\downloaded\webprice\webprice.xml' );
	logger($execution_id,'Deletando arquivo XML da Webprice. Status: '.$result);

	$result = unlink ( __DIR__.'\..\..\downloaded\webprice\webprice.csv' );
	logger($execution_id,'Deletando arquivo CSV da Webprice. Status: '.$result);

	//$result = $db->query("TRUNCATE <pricing.web_price>;");
	//logger($execution_id,'Truncando tabela temporária Webprice.');

	//Baixa o XML
	logger($execution_id,'Baixando XML da Webprice.',2);
	file_put_contents( __DIR__.'\..\..\downloaded\webprice\webprice.xml', fopen($db->get("params", "webprice_xml_url"),'r'));

	//Verifica o Download
	logger($execution_id,'Verificando download do XML da Webprice.',2);
	if ( file_exists( __DIR__.'\..\..\downloaded\webprice\webprice.xml' ) ){
    	logger($execution_id,'XML Baixado com sucesso.',2);
	} else {
	    logger($execution_id,'Erro ao baixar XML da Webprice.',2,1,1);
	    logger($execution_id,'Fim do processo.');
	    exit();
	}

	logger($execution_id,'Gerando base webprice CSV',2);

	$xml = file_get_contents(__DIR__.'\..\..\downloaded\webprice\webprice.xml');
	$Produtos = new SimpleXMLElement($xml);

	$arquivoCSV = fopen(__DIR__.'\..\..\downloaded\webprice\webprice.csv', 'a+');

	foreach ($Produtos as $Produto) {		

		foreach ($Produto->lojas->concorrente as $concorrente){


			if ($Produto->id_webglobal == null or $Produto->id_webglobal == ""){
				$id_webglobal = "null";
			}else{
				$id_webglobal = $Produto->id_webglobal;
			}
			if ($Produto->meu_codigo == null or $Produto->meu_codigo == ""){
				$meu_codigo = "null";
			}else{
				$meu_codigo = $Produto->meu_codigo;
			}
			if ($Produto->sku == null or $Produto->sku == ""){
				$sku = "null";
			}else{
				$sku = $Produto->sku;
			}
			if ($Produto->nome_produto == null or $Produto->nome_produto == ""){
				$nome_produto = "null";
			}else{
				$nome_produto  = $Produto->nome_produto;
			}
			if ($Produto->menor_preco == null or $Produto->menor_preco == ""){
				$menor_preco = "null";
			}else{
				$menor_preco = $Produto->menor_preco;
			}
			if ($Produto->maior_preco == null or $Produto->maior_preco == ""){
				$maior_preco = "null";
			}else{
				$maior_preco = $Produto->maior_preco;
			}
			if ($Produto->meu_preco == null or $Produto->meu_preco == ""){
				$meu_preco = "null";
			}else{
				$meu_preco = $Produto->meu_preco;
			}
			if ($Produto->diferenca == null or $Produto->diferenca == ""){
				$diferenca = "null";
			}else{
				$diferenca = $Produto->diferenca;
			}
			if ($concorrente->preco_avista == null or $concorrente->preco_avista == ""){
				$preco_avista = "null";
			}else{
				$preco_avista = $concorrente->preco_avista;
			}
			if ($concorrente->preco_por == null or $concorrente->preco_por == ""){
				$preco_por = "null";
			}else{
				$preco_por = $concorrente->preco_por;
			}
			if ($concorrente->loja == null or $concorrente->loja == ""){
				$loja = "null";
			}else{
				$loja = $concorrente->loja;
			}
			if ($concorrente->market_place == null or $concorrente->market_place == ""){
				$market_place = $concorrente->loja;
			}else{
				$market_place = $concorrente->market_place;
			}
			if ($concorrente->disponibilidade == null or $concorrente->disponibilidade == ""){
				$disponibilidade= "null";
			}else{
				$disponibilidade = $concorrente->disponibilidade;
			}
			if ($concorrente->url_loja == null or $concorrente->url_loja == ""){
				$url_loja = "null";
			}else{
				$url_loja = $concorrente->url_loja;
			}

			 
			 $linha = $id_webglobal.";".$meu_codigo.";".$sku.";".$nome_produto.";".$menor_preco.";".$maior_preco.";".$meu_preco.";".$diferenca.";".$preco_avista.";".$preco_por.";".$loja.";".$market_place.";".$disponibilidade.";".$url_loja.";1";

			 //Marketplaces ignorados
			 if ($market_place != "AMBIENT AIR" && 
				$market_place != "ANGELONI" && 
				$market_place != "ATUALSHOPPING" && 
				$market_place != "BEMOL" && 
				$market_place != "BOLEANA" && 
				$market_place != "BRASTEMP" && 
				$market_place != "CATRAL" && 
				$market_place != "CENTRAL AR" && 
				$market_place != "CERTEL" && 
				$market_place != "COLOMBO" && 
				$market_place != "CONSUL" && 
				$market_place != "COOKELETRORARO" && 
				$market_place != "CORRÊA BACK" && 
				$market_place != "CR DIEMENTZ" && 
				$market_place != "DSHOP" && 
				$market_place != "ELETROKASA" && 
				$market_place != "ELETROSOM" && 
				$market_place != "ELETRUM" && 
				$market_place != "FNAC" && 
				$market_place != "FRIGELAR" && 
				$market_place != "FRIOPECAS" && 
				$market_place != "GAZIN SHOP" && 
				$market_place != "HAVAN" && 
				$market_place != "JMAHFUZ" && 
				$market_place != "LEBES" && 
				$market_place != "LEROY MERLIN" && 
				$market_place != "LILIANI" && 
				$market_place != "LOJAS DULAR" && 
				$market_place != "LOJAS TAQI" && 
				$market_place != "LOJASMM" && 
				$market_place != "MANIA VIRTUAL" && 
				$market_place != "MEGAMAMUTE" && 
				$market_place != "MOBLY" && 
				$market_place != "MULTI AR" && 
				$market_place != "MULTILOJA" && 
				$market_place != "NOVO MUNDO" && 
				$market_place != "ONOFRE ELETRO" && 
				$market_place != "RABELO" && 
				$market_place != "SCHUMANN" && 
				$market_place != "SHOPFÁCIL" && 
				$market_place != "SHOPFATO" && 
				$market_place != "STRAR" && 
				$market_place != "SUPERMUFFATO" && 
				$market_place != "ÚNICA AR CONDICIONADO" && 
				$market_place != "WEB CONTINENTAL" && 
				$market_place != "ZEMA" ){

					fwrite($arquivoCSV, $linha.PHP_EOL);
			 }
	
			
		}
	}
	fclose($arquivoCSV);


