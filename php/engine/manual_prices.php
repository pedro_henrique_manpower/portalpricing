<?php
										//include '../connection.php'; //Info de conexão

	//Limpa as regras dde preço manual que já expiraram
	$db->query("update preco_manual set ativo = 0 where end_date < now() and ativo = 1");


	//Contador de skus manuais processados
	$contador = 0;
	
	//Total de SKUs ativos
	$nroPrecos = $db->count('preco_manual',['ativo' => 1]);
	$precos = $db->select("preco_manual",['ambiente','sku','modelo','politica','vtex_id','price','user_changed'],['ativo'=> 1]);
	
	//Para cada SKU, pega o preço manual e updateia na base
	logger($execution_id,'Preenchendo preços manuais.',3);
	foreach($precos as $preco){
		
		$contador++;

		$db->query('update preco_sugerido set preco = '.$preco['price'].', log_msg = concat(log_msg, \'Acatando preço manual ( R$' . $preco['price'] . ' ) definido pelo usuário ID '.$preco['user_changed'].'. \') where ambiente = '.$preco['ambiente'].' and politica = '.$preco['politica'].' and sku = \''.$preco['sku'].'\' AND execution_id = \''.$execution_id.'\';');
	}