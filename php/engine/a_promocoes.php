<?php
	//include '../connection.php'; //Info de conexão


	logger($execution_id,'Inicializando tabela de promoções.',3);
	
	//Monta o array de ambientes
	$listaPoliticas = $db->select("politicas",['id','nome','ambiente','saleschannel','marca'],['ativo' => 1]);	
	echo "\n";
	foreach($listaPoliticas as $politica){	

		//Contador 
		$contador = 0;
		
		//Total de SKUs ativos
		$nroSKUs = $db->count('sku',['ativo' => 1]);
		$skus = $db->select("sku",['sku','modelo','marca','vtex_id','ambiente','usar_bayes','preco_tabela','trava_minima','trava_maxima'],['ativo'=> 1, 'ambiente' => $politica['ambiente'] ]);
		
		foreach($skus as $sku){
			$contador++;

			//echo "\rPreenchendo SKU ".$contador." de ".$nroSKUs." para politica ".$politica['nome']."<br>";


			if ($politica['marca'] == $sku['marca'] || $politica['marca'] ==  "Todas"){

				//echo "=============<br>".."<br>=============";


				//COLUNAS DE BENCHMARKS

				$preco_minimo = $db->min('web_price','menor_preco',['sku' => $sku['sku']]);
				$preco_maximo = $db->max('web_price','maior_preco',['sku' => $sku['sku']]);

				if($sku['marca'] == "Brastemp"){
					$site_aberto = $db->get('web_price','preco_por',['sku' => $sku['sku'],'loja' => 'BRASTEMP','execution_id' => $execution_id]);
				}elseif($sku['marca'] == "Consul"){
					$site_aberto = $db->get('web_price','preco_por',['sku' => $sku['sku'],'loja' => 'CONSUL','execution_id' => $execution_id]);
				}else{
					$site_aberto = 0;
				}

				$preco_tabela = $sku['preco_tabela'];
				$trava_min = $sku['trava_minima'];
				$trava_max = $sku['trava_maxima'];
				$varejista = 0;
				$buybox = 0;


				$db->insert('preco_sugerido',[	'execution_id'		=>	$execution_id,
												'ambiente'			=>	$politica['ambiente'],
												'politica'			=>	$politica['id'],
												'vtex_id'			=>	$sku['vtex_id'],
												'modelo'			=>	$sku['modelo'],
												'usar_bayes'		=>	$sku['usar_bayes'],
												'preco'				=>	$sku['preco_tabela'],
												'buybox'			=>	$buybox,
												'varejista'			=>	$varejista,
												'preco_tabela'		=>	$preco_tabela,
												'preco_site_aberto'	=>	$site_aberto,
												'menor_preco'		=>	$preco_minimo,
												'maior_preco'		=>	$preco_maximo,
												'trava_minima'		=>	$trava_min,
												'trava_maxima'		=>	$trava_max,
												'sku'				=>	$sku['sku']]);
			}
		}
	}

