<?php
										//include '../connection.php'; //Info de conexão


	//Contador
	$RScontador = $db->query("
      	SELECT COUNT(distinct(<preco_sugerido.sku>)) as total
		FROM <preco_sugerido>
		JOIN <bayes> ON <preco_sugerido.sku> = <bayes.sku> and <preco_sugerido.ambiente> = <bayes.ambiente>
		WHERE <preco_sugerido.usar_bayes> IS TRUE")->fetchAll();

	foreach($RScontador as $linha){
		$total = $linha['total'];
	}


	//Total de SKUs com preço bayes 
	$precosBayes = $db->query("
      	SELECT DISTINCT(concat(<preco_sugerido.sku>,<preco_sugerido.ambiente>)) as chave, preco_sugerido.sku, <preco_sugerido.ambiente>, <preco_sugerido.vtex_id>, <preco_sugerido.preco>, <bayes.preco_otimo>
		FROM <preco_sugerido>
		JOIN <bayes> ON <preco_sugerido.sku> = <bayes.sku> and <preco_sugerido.ambiente> = <bayes.ambiente>
		WHERE <preco_sugerido.usar_bayes> IS TRUE")->fetchAll();	
	
	$contador = 0;
	echo "\n";
	foreach($precosBayes as $preco){
		
		$contador++;

		//var_dump($preco);
		echo "\rInserindo preco Bayes ".$contador." de ".$total . " SKU ".$preco['sku']."<br>";
		$db->query('UPDATE preco_sugerido SET log_msg = CONCAT(log_msg, \'Aplicando preço Bayes (R$ ' . $preco['preco_otimo'] . '). \'),  preco = ' . $preco['preco_otimo'] . ' WHERE ambiente = ' . $preco['ambiente'] . ' and sku = \'' . $preco['sku'] . '\' AND execution_id = \''.$execution_id.'\';');

		//var_dump($db->error());
	}