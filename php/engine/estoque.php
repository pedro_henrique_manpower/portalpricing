<?php
	//include '../connection.php'; //Info de conexão
	
	//Monta o array de ambientes
	$listaAmbientes = $db->select("ambientes",['id','vtex_key','vtex_token','api_url','short_name']);	
	
	foreach($listaAmbientes as $ambiente){
		$keys[$ambiente['id']] = $ambiente['vtex_key'];
		$tokens[$ambiente['id']] = $ambiente['vtex_token'];
		$shortname[$ambiente['id']] = $ambiente['short_name'];
	}
	

	//Contador de skus processados
	$contador = 0;
	
	//Total de SKUs ativos
	$nroSKUs = $db->count('sku',['ativo' => 1]);
	$skus = $db->select("sku",['id','sku','vtex_id','ambiente'],['ativo'=> 1]);
	
	//Para cada SKU, pega o estoque
	echo "\n<br>";
	foreach($skus as $sku){
		
		$estoque = 0;
		$reserva = 0;
		$contador++;

		//Monta header do admin (ambiente)
		$header = array('X-VTEX-API-AppKey' => $keys[$sku['ambiente']],'X-VTEX-API-AppToken' => $tokens[$sku['ambiente']]);

		echo "Preenchendo estoque de SKU ".$contador." de ".$nroSKUs."<br>";
		
		$endpoint = "http://logistics.vtexcommercestable.com.br/api/logistics/pvt/inventory/skus/".$sku['vtex_id']."?an=".$shortname[$sku['ambiente']];

		do{
			$response = Unirest\Request::get($endpoint, $header);
			
			if($response->code != "200"){
				echo "  Erro de requisição: " . $response->code . " - Esperando 10s.\n";
				sleep(10);
			}
		}while ($response->code != "200");
		
		
		//Soma quantindades de estoque e reserva
		foreach($response->body->balance as $warehouse){
			switch($warehouse->warehouseId):
				case "7100": case "7200": case "7300": 				
					$estoque = $estoque + $warehouse->totalQuantity;
					$reserva = $reserva + $warehouse->reservedQuantity;
			endswitch;
		}

		$db->insert('estoque',['sku'=>$sku['sku'],'ambiente'=>$sku['ambiente'],'estoque'=>$estoque,'reserva'=>$reserva,'execution_id'=>$execution_id]);
	}
	
