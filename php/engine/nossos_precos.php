<?php
	//include '../connection.php'; //Info de conexão
	//set_time_limit(0);
	//ob_implicit_flush(true);
	
	//Monta o array de ambientes
	$listaAmbientes = $db->select("ambientes",['id','vtex_key','vtex_token','api_url','short_name']);	
	
	foreach($listaAmbientes as $ambiente){
		$keys[$ambiente['id']] = $ambiente['vtex_key'];
		$tokens[$ambiente['id']] = $ambiente['vtex_token'];
		$shortname[$ambiente['id']] = $ambiente['short_name'];
	}
	 

	//Contador de skus processados
	$contador = 0;
	
	//Total de SKUs ativos
	$nroSKUs = $db->count('sku',['ativo' => 1]);
	$skus = $db->select("sku",['id','sku', 'marca' ,'vtex_id','ambiente'],['ativo'=> 1]);
	
	//Para cada SKU, pega o preço
	//echo "\n<br>";
	foreach($skus as $sku){
		$estoque = 0;
		$reserva = 0;
		$contador++;

		//Monta header do admin (ambiente)
		$header = array('X-VTEX-API-AppKey' => $keys[$sku['ambiente']],'X-VTEX-API-AppToken' => $tokens[$sku['ambiente']]);

		echo "Preenchendo nosso preço - SKU ".$contador." de ".$nroSKUs."<br>";
		

		if($shortname[$sku['ambiente']]=='whirlpool'){
			if($sku['marca']=="Brastemp"){
				$endpoint = "https://rnb.vtexcommercestable.com.br/api/pricing/pvt/price-sheet/".$sku['vtex_id']."/2/?an=".$shortname[$sku['ambiente']];
			}else{
				$endpoint = "https://rnb.vtexcommercestable.com.br/api/pricing/pvt/price-sheet/".$sku['vtex_id']."/3/?an=".$shortname[$sku['ambiente']];
			}
		}else{//compra certa
			$endpoint = "https://rnb.vtexcommercestable.com.br/api/pricing/pvt/price-sheet/".$sku['vtex_id']."/1/?an=".$shortname[$sku['ambiente']];
		}


		do{
			$response = Unirest\Request::get($endpoint, $header);
			
			if($response->code != "200"){
				echo "  Erro de requisição: " . $response->code . " - Esperando 10s.\n";
				sleep(10);
			}
		}while ($response->code != "200");
		
		//Encontra o preço base praticado (vencimento em 2125) e envia para o banco
		foreach($response->body as $preco){

			if( ($shortname[$sku['ambiente']]=='whirlpool' && !isset($preco->internalCampaign) && substr($preco->validTo,0,4) == "2125" ) ||
				($shortname[$sku['ambiente']]=='compracerta' && $preco->internalCampaign == "corporativo" && substr($preco->validTo,0,4) == "2125" )
			){

				$db->query("UPDATE `preco_sugerido` SET `preco_site_aberto` = ".doubleval($preco->price)." WHERE `execution_id` = ".intval($execution_id)." AND `ambiente` = ".intval($sku['ambiente'])." AND `vtex_id` = ".$preco->itemId);
				//var_dump( $db->last() );

			}
		}
	}
	
