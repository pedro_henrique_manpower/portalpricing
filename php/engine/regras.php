<?php
										

	// ---------------- CALCULO DAS REGRAS ----------------------------------------->

	//Inicializa as queries
	$db->query("UPDATE REGRAS SET query = CONCAT('UPDATE preco_sugerido set preco = ',fator,' * ') ");



	//Insere o multiplicador e o benchmark

	//BuyBox
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'buybox,  precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator ao BuyBox. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' )  where benchmark = 7 ");

	//varejista
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'varejista , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator ao Varejista. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 8");

	//Trava Mínima
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'trava_minima , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator a Trava Minima. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 9");

	//trava máxima
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'trava_maxima , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator a Trava Maxima. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 10");

	//Preço tabela
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'preco_tabela , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator ao Preço Tabela. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 11");

	// Preco Sites abertos
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'preco_site_aberto , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator ao Preço Site Aberto. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 12");
	
	//maior preço
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'maior_preco  , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator ao Maior Preço. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 13");

	//Menor preço
	$db->query("UPDATE REGRAS SET query = CONCAT(query, 'menor_preco , precificado = 1, log_msg = concat(log_msg,\' Aplicando Fator ao Menor Preço. \') where execution_id = " . $execution_id . " and ambiente = ',ambiente, ' and politica = ', politica, ' and sku = \'', sku ,'\'' ) where benchmark = 14");



  	$queries = $db->query("
      SELECT 
      <regras.id>,
      <regras.query>
      FROM <regras>"
    )->fetchAll();

  	//Exercuta as queries dinamicas
	logger($execution_id,'Rodando Queries Dinâmicas de Regras de Preço.',3);
	foreach($queries as $query){
		$db->query($query['query']);
	}

	//Arredonda os preços
	logger($execution_id,'Arredondando preços não-UTM para final 9.',3);
	$db->query("update pricing.preco_sugerido SET preco = ((CEIL((round(preco,0)/10))*10)-1) WHERE pricing.preco_sugerido.execution_id = ".$execution_id);


	//Normalização de preços

	$normalizacao = $db->get("params","normalizacao");
	logger($execution_id,'Normalizando preços. Flag: '.$normalizacao,3);


	//$politicas = $db->query("politicas",['id'],['ativo'=> 1]);
	$politicas = $db->query("SELECT id from politicas where ativo = 1")->fetchAll();
	$modelos = $db->query("SELECT distinct(modelo) from sku where ativo = 1")->fetchAll();
	
	foreach($politicas as $politica){
		foreach($modelos as $modelo){

			//echo "Normalizando política " . $politica['id'] . ":". $modelo['modelo']."<br>";

			if ($normalizacao == 1){
				$db->query('UPDATE preco_sugerido SET preco = ( SELECT min(preco) FROM (SELECT * FROM preco_sugerido) p WHERE p.execution_id = '.$execution_id.' AND p.politica = '.$politica['id'].' AND p.modelo = \''.$modelo['modelo'].'\' ), log_msg = concat(log_msg,\' Normalizando pelo menor preço. \') WHERE execution_id = '.$execution_id.' AND politica = '.$politica['id'].' AND modelo = \''.$modelo['modelo'].'\'');
			}

			if ($normalizacao == 2){
				$db->query('UPDATE preco_sugerido SET preco = ( SELECT avg(preco) FROM (SELECT * FROM preco_sugerido) p WHERE p.execution_id = '.$execution_id.' AND p.politica = '.$politica['id'].' AND p.modelo = \''.$modelo['modelo'].'\' ), log_msg = concat(log_msg,\' Normalizando pela média. \') WHERE execution_id = '.$execution_id.' AND politica = '.$politica['id'].' AND modelo = \''.$modelo['modelo'].'\'');
			}

			if ($normalizacao == 3){
				$db->query('UPDATE preco_sugerido SET preco = ( SELECT max(preco) FROM (SELECT * FROM preco_sugerido) p WHERE p.execution_id = '.$execution_id.' AND p.politica = '.$politica['id'].' AND p.modelo = \''.$modelo['modelo'].'\' ), log_msg = concat(log_msg,\' Normalizando pelo maior preço. \') WHERE execution_id = '.$execution_id.' AND politica = '.$politica['id'].' AND modelo = \''.$modelo['modelo'].'\'');
			}
		}
	}
	
	