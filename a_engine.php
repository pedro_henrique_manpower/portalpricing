<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<?php

	/*
	Engine de Cálculo - Compra Certa - D2C
	Desenvolvida pela equipe de Produtividade e Performance/IPS do Compra Certa.
	 */

	header ('Content-type: text/html; charset=utf-8');
	set_time_limit(0);
	ob_implicit_flush(true);
	error_reporting(E_ALL & ~E_NOTICE);
	session_start();

	
	include __DIR__.'\php\connection.php'; //Info de conexão
	
// -------------------- INICIALIZAÇÕES --------------------------

	//Usuário executor do processo
	if(isset($_SESSION['user_id'])){
		$usuario = $_SESSION['user_id'];
	}else{
		$usuario = 1;
	}
	
	$dataInicial = date('Y-m-d H:i:s');

	$db->insert('execution',['start' => $dataInicial,'hash' => md5($dataInicial),'user' => $usuario,'tipo' => 'alavanca']);
	$execution_id = $db->id();
	logger($execution_id,'Iniciando processo de precificação.',1,$usuario,1);	
	
	
	exit;
	

	//Preenche SKUs em promoção
	require_once(__DIR__.'\php\engine\a_promocoes.php');
	
	
	

	//Encerrando Execução
	$db->update('execution',['end' => date('Y-m-d H:i:s')],['id' => $execution_id]);
	logger($execution_id,'Fim do processo.',2);

	
?>

</body>
</html>