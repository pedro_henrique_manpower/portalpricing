<?php 
session_start();

if(!isset($_SESSION['user_id'])){
  header('location:login.php');
}

	 include dirname(__FILE__)."\php\connection.php"; //Info de conexão

   if( isset($_GET['e']) && isset($_GET['id'])){

     $resultSet = $db->query("
      SELECT 
      <politicas.id>,
      <politicas.nome>,
      <politicas.loja_webprice>,
      <politicas.ambiente>,
      <politicas.saleschannel>
      FROM <politicas>
      WHERE <politicas.id> = " . $_GET['id']
    )->fetchAll();

   }else{
     $resultSet = $db->query("
      SELECT 
      <politicas.id>,
      <politicas.ambiente>,
      <politicas.loja_webprice>,
      <politicas.nome>,
      <politicas.saleschannel>,
      <ambientes.nome> as <nomeAmbiente>
      FROM <politicas>
      LEFT JOIN <ambientes> ON <politicas.ambiente> = <ambientes.id>"
    )->fetchAll(); 
   }

   $ambientes = $db->query("
    SELECT 
    <ambientes.id>,
    <ambientes.nome>
    FROM <ambientes>"
  )->fetchAll();

  ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Portal de Pricing versão 3.0">
    <meta name="author" content="Produtividade e Perfomance D2C ">

    <title>Portal de Pricing D2C</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">

    <?php include "php/navigation.php";?>  

    <!-- CONTEÚDO -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Home</a>
          </li>
          <li class="breadcrumb-item active">Políticas Comerciais</li>
        </ol>

        <!-------------------------- CONTEUDO -------------------------------->

        <?php if( isset($_GET['e']) && isset($_GET['id'])): ?>
        <?php foreach($resultSet as $linha): ?>
         <form action="php/cadastrar.php" method="POST">
           <div class="form-group row">
            <label for="nome_politica" class="col-5 col-form-label">Nome da Política</label> 
            <div class="col-7">
              <input id="nome_politica" name="nome_politica" placeholder="Nome" type="text" required="required" class="form-control here" value="<?php echo $linha['nome']; ?>">
            </div>
          </div>
          <div class="form-group row">
            <label for="ambiente" class="col-5 col-form-label">Ambiente</label> 
            <div class="col-7">
              <select id="ambiente" name="ambiente" required="required" class="custom-select">
               <?php foreach($ambientes as $ambiente): ?>
                <option <?php echo ($linha['ambiente']==$ambiente['id'])?'selected':''; ?> value="<?php echo $ambiente['id'];?>"><?php echo $ambiente['nome'];?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="vtex_saleschannel" class="col-5 col-form-label">ID Vtex (Sales Channel)</label> 
          <div class="col-7">
            <input id="vtex_saleschannel" name="vtex_saleschannel" type="text" class="form-control here" value="<?php echo $linha['saleschannel']?>">
          </div>
        </div> 
        <div class="form-group row">
          <label for="loja_webprice" class="col-5 col-form-label">Loja WebPrice (Marketplace Base)</label> 
          <div class="col-7">
            <input id="loja_webprice" name="loja_webprice" type="text" class="form-control here" value="<?php echo $linha['loja_webprice']?>">
          </div>
        </div> 
        <div class="form-group row">
          <div class="offset-5 col-7">
            <button name="submit" type="submit" class="btn btn-primary">Atualizar</button>
          </div>
        </div>
        <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
        <input type="hidden" name="politica_id" value="<?php echo $_GET['id']; ?>"/>
        <input type="hidden" name="atualizar" value="1"/>
      </form>
    <?php endforeach; ?>
    <?php else: ?> <!-- LISTAR -->

    <a href="#" class="nav-link" data-toggle="modal" data-target="#modal">
      <i class="fa fa-plus-circle" aria-hidden="true"></i> Nova política
    </a>

    <table class="table table-bordered nowrap" id="tabela" width="100%" cellspacing="0">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Ambiente</th>
          <th>Sales Channel</th>
        </tr>
      </thead>

      <tbody>
        <?php foreach($resultSet as $linha): ?>
          <tr>
            <td><a href="politicas.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['nome']; ?></a></td>
            <td><a href="politicas.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['nomeAmbiente']; ?></a></td>
            <td><a href="politicas.php?e=1&id=<?php echo $linha['id']; ?>"><?php echo $linha['saleschannel']; ?></a></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>

  <!-------------------------- /CONTEUDO -------------------------------->


  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  <footer class="sticky-footer">
    <div class="container">
      <div class="text-center">
        <small>Desenvolvido por: Produtividade e Performance | Compra Certa | D2C </small>
      </div>
    </div>
  </footer>
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
  </a>

  <!-- Modal Novo Cadastro -->
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="labelModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelModal">Nova Política</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="php/cadastrar.php" method="POST">
            <div class="form-group row">
              <label for="nome_politica" class="col-3 col-form-label">Nome</label> 
              <div class="col-9">
                <input id="nome_politica" name="nome_politica" placeholder="Ex: B2W Brastemp" type="text" class="form-control here" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="api_url" class="col-3 col-form-label">Ambiente</label> 
              <div class="col-9">
               <select id="ambiente" name="ambiente" required="required" class="custom-select">
                 <?php foreach($ambientes as $ambiente): ?>
                  <option value="<?php echo $ambiente['id'];?>"><?php echo $ambiente['nome'];?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="vtex_saleschannel" class="col-3 col-form-label">Sales Channel</label> 
            <div class="col-9">
              <input id="vtex_saleschannel" name="vtex_saleschannel" placeholder="Ex: 11" type="text" class="form-control here" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="loja_webprice" class="col-3 col-form-label">Loja WebPrice (Marketplace Base)</label> 
            <div class="col-9">
              <input id="loja_webprice" name="loja_webprice" placeholder="Ex: BRASTEMP" type="text" class="form-control here" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="offset-5 col-9">
              <button name="submit" type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
          </div>
          <input type="hidden" name="tipoDeCadastro" value="<?php echo basename($_SERVER["SCRIPT_FILENAME"]); ?>"/>
          <input type="hidden" name="atualizar" value="0"/>
        </form>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Modal Sucesso -->
<div class="modal fade" id="modalSucesso" tabindex="-1" role="dialog" aria-labelledby="labelModalSucesso" aria-hidden="true">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <h5 class="modal-title" id="labelModalSucesso">Política</h5>
       <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
         <span aria-hidden="false">×</span>
       </button>
     </div>
     <div class="modal-body">
       Politica salva com sucesso!          
     </div>
     <div class="modal-footer">
     </div>
   </div>
 </div>
</div>




<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>
<!-- Custom scripts for this page-->
<script src="js/sb-admin-datatables.min.js"></script>
<script src="js/sb-admin-charts.min.js"></script>

<!-- ATIVA O MODAL DE SUCESSO NO CADASTRO -->
<?php if(isset($_GET['s'])) : ?>

  <script type="text/javascript">
    $(document).ready(function(){
      $("#modalSucesso").modal('show');
    });
  </script>


<?php endif; ?>
</div>
</body>

</html>
